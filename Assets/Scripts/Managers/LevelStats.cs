﻿using UnityEngine;
using System.Collections;

public class LevelStats : MonoBehaviour
{

    #region Components
    protected AudioSource niceNiceSound;
    protected AudioSource fantasticSound;
    protected AudioSource acceptableSound;

    #endregion

    protected int niceNice;
    protected int fantastic;
    protected int acceptable;

    protected int niceNiceCounter;
    protected int fantasticCounter;
    protected int acceptableCounter;

    public int NumNiceNice
    {
        get { return niceNice; }
        set
        {
            niceNiceCounter += 1;
            niceNice = value;
        }
    }

    public int NumFantastic
    {
        get { return fantastic; }
        set
        {
            fantasticCounter += 1;
            fantastic = value;
        }
    }

    public int NumAcceptable
    {
        get { return acceptable; }
        set
        {
            acceptableCounter += 1;
            acceptable = value;
        }
    }

    public int NumSloppy
    {
        get;
        set;
    }

    public float Money
    {
        get;
        set;
    }

    public int NumSpiders
    {
        get;
        set;
    }

    public int NumMoleMen
    {
        get;
        set;
    }

    public int TotalServed
    {
        get { return NumNiceNice + NumFantastic + NumAcceptable + NumSloppy; }
    }

	// Use this for initialization
	void Start () 
    {
        niceNiceSound = this.transform.Find("Sounds").Find("NiceNiceSound").GetComponent<AudioSource>();
        fantasticSound = this.transform.Find("Sounds").Find("FantasticSound").GetComponent<AudioSource>();
        acceptableSound = this.transform.Find("Sounds").Find("AcceptableSound").GetComponent<AudioSource>();
  	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (niceNiceCounter > 5 && !fantasticSound.isPlaying && !acceptableSound.isPlaying)
        {
            niceNiceCounter = 0;
            niceNiceSound.Play();
        }

        if (fantasticCounter > 3 && !niceNiceSound.isPlaying && !acceptableSound.isPlaying)
        {
            fantasticCounter = 0;
            fantasticSound.Play();
        }

        if (acceptableCounter > 3 && !niceNiceSound.isPlaying && !fantasticSound.isPlaying)
        {
            acceptableCounter = 0;
            acceptableSound.Play();
        }
	}
}

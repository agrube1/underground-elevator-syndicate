﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class LevelFactory : MonoBehaviour
{

    public Transform elevator;
    public Transform lizard;
    public Transform elevatorNode;
    public Transform elevatorLight;
    public Transform elevatorPully;
    public Transform door;

    protected Transform elevatorLayer;
    protected Transform elevatorNodeLayer;
    protected Transform lizardLayer;
    protected Transform bgNearLayer;

    protected Dictionary<int, List<Node>> elevators;

    protected LevelProperties levelProperties;

	// Use this for initialization
	void Start () 
    {
        elevatorLayer = this.transform.Find("ElevatorLayer");
        elevatorNodeLayer = this.transform.Find("ElevatorNodeLayer");
        lizardLayer = this.transform.Find("LizardLayer");
        bgNearLayer = this.transform.Find("BgNearLayer");

        elevators = new Dictionary<int, List<Node>>();
        levelProperties = GetComponent<LevelProperties>();
        CreateLevel();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    protected void CreateLevel()
    {
        TextAsset textAsset = (TextAsset)Resources.Load("Levels/test", typeof(TextAsset));
        XmlDocument level = new XmlDocument();
        level.LoadXml(textAsset.text);

        // get the floors
        XmlNodeList xmlFloors = level.GetElementsByTagName("Floor");
        foreach (XmlNode xmlNode in xmlFloors)
        {
            XmlNodeList allNodes = xmlNode.ChildNodes;
            int floorNumber = int.Parse(allNodes[0].InnerText);
            float y = float.Parse(allNodes[1].InnerText, System.Globalization.NumberStyles.Float);
            float xMin = float.Parse(allNodes[2].InnerText, System.Globalization.NumberStyles.Float);
            float xMax = float.Parse(allNodes[3].InnerText, System.Globalization.NumberStyles.Float);

            if (!levelProperties.floors.ContainsKey(floorNumber))
            {
                Vector3 leftNode = new Vector3(xMin, y, 0);
                Vector3 rightNode = new Vector3(xMax, y, 0);

                Transform newDoor1 = (Transform)Instantiate(door);
                newDoor1.parent = bgNearLayer;
                Vector3 doorPosition1 = new Vector3();
                doorPosition1.x = leftNode.x;
                doorPosition1.y = leftNode.y;
                doorPosition1.z = newDoor1.parent.position.z;

                newDoor1.position = doorPosition1;
                newDoor1.name = "Door 1-" + floorNumber;

                Transform newDoor2 = (Transform)Instantiate(door);
                newDoor2.parent = bgNearLayer;
                Vector3 doorPosition2 = new Vector3();
                doorPosition2.x = rightNode.x;
                doorPosition2.y = rightNode.y;
                doorPosition2.z = newDoor1.parent.position.z;

                newDoor2.position = doorPosition2;
                newDoor2.name = "Door 2-" + floorNumber;

                Floor floorObj = new Floor();
                floorObj.floorNumber = floorNumber;
                floorObj.leftNode = new Node(leftNode, 0, floorNumber);
                floorObj.rightNode = new Node(rightNode, 0, floorNumber);

                levelProperties.floors.Add(floorNumber, floorObj);
            }
        }

        // get the elevators
        XmlNodeList xmlElevators = level.GetElementsByTagName("Elevator");
        foreach (XmlNode xmlNode in xmlElevators)
        {
            XmlNodeList allNodes = xmlNode.ChildNodes;
            float x = float.Parse(allNodes[0].InnerText, System.Globalization.NumberStyles.Float);
            string supportedFloors = allNodes[1].InnerText;

            // create the elevator object.
            Transform elevatorPrefab = (Transform)Instantiate(elevator);
            Elevator elevatorObj = elevatorPrefab.GetComponent<Elevator>();
            elevatorObj.nodes = new List<Node>();
            elevatorObj.nodeFloorMap = new Dictionary<int, int>();

            foreach (string s in supportedFloors.Split(','))
            {
                int floorNumber = int.Parse(s);
                Node node = new Node(new Vector3(x, levelProperties.floors[floorNumber].Y, 0), levelProperties.elevators.Count, floorNumber);
                elevatorObj.nodeFloorMap.Add(elevatorObj.nodes.Count, floorNumber);
                elevatorObj.nodes.Add(node);
                Transform newElevatorNode = (Transform)Instantiate(elevatorNode);
                newElevatorNode.parent = elevatorNodeLayer;
                Vector3 elevatorPos = new Vector3(node.NodePosition.x,
                                                  node.NodePosition.y, newElevatorNode.parent.position.z);
                newElevatorNode.position = elevatorPos;
                if (floorNumber == 0)
                {
                    Transform newLizard = (Transform)Instantiate(lizard);
                    newLizard.GetComponent<LizardController>().elevatorNum = levelProperties.elevators.Count;
                    newLizard.parent = lizardLayer;
                    Vector3 lizardPosition = newElevatorNode.transform.position;
                    lizardPosition.x += 65;
                    lizardPosition.y -= 20;
                    lizardPosition.z = newLizard.parent.position.z;
                    newLizard.position = lizardPosition;
                    newLizard.name = "Lizard " + levelProperties.elevators.Count;
                }
                else
                {
                    Transform newElevatorLight = (Transform)Instantiate(elevatorLight);
                    newElevatorLight.parent = newElevatorNode;
                    Vector3 lightPosition = newElevatorNode.transform.position;
                    lightPosition.x += 65;
                    lightPosition.y += 20;
                    lightPosition.z = newElevatorLight.parent.position.z;
                    newElevatorLight.position = lightPosition;
                    node.ElevatorLight = newElevatorLight.GetComponent<ElevatorLightController>();
                }
            }

            elevatorObj.transform.parent = elevatorLayer;
            elevatorObj.transform.position = new Vector3(elevatorObj.nodes[0].NodePosition.x, 
                elevatorObj.nodes[0].NodePosition.y, elevatorObj.transform.parent.position.z);
            elevatorObj.transform.name = "Elevator " + levelProperties.elevators.Count;

            Transform newElevatorPully = (Transform)Instantiate(elevatorPully);
            newElevatorPully.parent = elevatorLayer;
            newElevatorPully.name = "Elevator Pully " + levelProperties.elevators.Count;
            newElevatorPully.GetComponent<ElevatorPullyController>().elevator = elevatorObj;
            newElevatorPully.transform.position = new Vector3(
                elevatorObj.nodes[elevatorObj.nodes.Count - 1].NodePosition.x,
                elevatorObj.nodes[elevatorObj.nodes.Count - 1].NodePosition.y + elevatorObj.collider.bounds.extents.y + 15,
                elevatorObj.transform.parent.position.z);

            // add the elevator to the level properties dictionary.
            levelProperties.elevators.Add(levelProperties.elevators.Count, elevatorObj);

        }
    }

    protected void ClearLevel()
    {

    }

    public void OnLevelLoad()
    {

    }
}

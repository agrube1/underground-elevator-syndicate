﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelProperties : MonoBehaviour
{

    #region Constants
    protected const float NICE_NICE_REWARD = 25;
    protected const float FANTASTIC_REWARD = 17.50f;
    protected const float ACCEPTABLE_REWARD = 8.75f;
    protected const float SLOPPY_REWARD = 1;

    public const float MAX_PARANOIA = 500;
    public const float SAFE_MAX = .45f;
    public const float MEDIUM_MAX = .70f;
    public const float ALARM_LEVEL = MEDIUM_MAX * MAX_PARANOIA;

    protected const int INCREMENT_TIME = 5;

    protected const int FIVE = 500;
    protected const int SIX = 600;
    protected const int SEVEN = 700;
    protected const int EIGHT = 800;
    protected const int NINE = 900;
    protected const int TEN = 1000;
    protected const int ELEVEN = 1100;
    protected const int TWELVE = 1200;
    protected const int THIRTEEN = 1300;
    protected const int FOURTEEN = 1400;
    protected const int FIFTEEN = 1500;
    protected const int SIXTEEN = 1600;
    protected const int SEVENTEEN = 1700;
    protected const int EIGHTEEN = 1800;
    protected const int NINETEEN = 1900;

    protected const float DEAD_RATE = 12f;
    protected const float BASE_RATE = 8f;
    protected const float NORMAL_RATE = 4.5f;
    protected const float BUSY_RATE = 2.5f;
    protected const float OVERWHELM_RATE = 1.5f;

    #endregion

    protected AudioSource doExistSound;
    protected AudioSource gameOverSound;
    protected AudioSource winSound;
    protected AudioSource backgroundMusic;
    protected AudioSource alarmSound;
    protected AudioSource niceNiceSound;
    protected AudioSource fantasticSound;
    protected AudioSource acceptableSound;
    protected AudioSource sloppySound;

    protected LevelStats levelStats;

    protected GameOverController gameOverController;
    protected TweenAlpha bgTweenAlpha;

    public Dictionary<int, Elevator> elevators;
    public Dictionary<int, Floor> floors;

    protected float money;
    protected float paranoia;
    protected float patronSendDelay;
    protected float spiderSendDelay;
    protected float moleSendDelay;

    protected int time;
    protected int tens;
    protected float dt;
    protected float rate;

    protected bool gameOver;
    protected bool win;
    protected bool pause;
    protected bool done;

    #region Properties
    public int NumElevators
    {
        get { return elevators.Keys.Count; }
    }

    public int NumFloors
    {
        get { return floors.Keys.Count; }
    }

    public float Money
    {
        get { return money; }
    }

    public float Paranoia
    {
        get { return paranoia; }
    }

    public bool GameOver
    {
        get { return gameOver; }
    }

    public bool Win
    {
        get { return win; }
    }

    public bool Pause
    {
        get { return pause; }
    }

    public int GameTime
    {
        get { return time; }
    }

    public bool Done
    {
        get { return done; }
    }

    public float PatronRate
    {
        get { return patronSendDelay; }
    }

    public float SpiderRate
    {
        get { return spiderSendDelay; }
    }

    #endregion

    // Use this for initialization
	void Awake () 
    {
        time = FIVE;
        elevators = new Dictionary<int, Elevator>();
        floors = new Dictionary<int, Floor>();
        money = 0;
        gameOver = false;
        win = false;
        done = false;

        levelStats = GetComponent<LevelStats>();

        gameOverController = this.transform.Find("GuiNearLayer").Find("GameOver").GetComponent<GameOverController>();

        doExistSound = this.transform.Find("Sounds").Find("DoExistSound").GetComponent<AudioSource>();
        winSound = this.transform.Find("Sounds").Find("WinSound").GetComponent<AudioSource>();
        gameOverSound = this.transform.Find("Sounds").Find("GameOverSound").GetComponent<AudioSource>();
        backgroundMusic = this.transform.Find("Sounds").Find("BackgroundMusic").GetComponent<AudioSource>();
        alarmSound = this.transform.Find("Sounds").Find("AlarmSound").GetComponent<AudioSource>();

        niceNiceSound = this.transform.Find("Sounds").Find("NiceNiceWinSound").GetComponent<AudioSource>();
        fantasticSound = this.transform.Find("Sounds").Find("FantasticWinSound").GetComponent<AudioSource>();
        acceptableSound = this.transform.Find("Sounds").Find("AcceptableWinSound").GetComponent<AudioSource>();
        sloppySound = this.transform.Find("Sounds").Find("SloppyWinSound").GetComponent<AudioSource>();

        time = FIVE;
        tens = 0;
	}

    void Start()
    {
        bgTweenAlpha = this.transform.Find("BgFarLayer").Find("BackgroundNight").GetComponent<TweenAlpha>();
        bgTweenAlpha.Fade(bgTweenAlpha.GetComponent<tk2dSprite>().color.a, 0, .012f);
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (!gameOver && !win)
        {
            if (!pause)
            {
                IncrementTime();
                SetCurrentRate();
            }

            if (Paranoia > MAX_PARANOIA)
            {
                gameOver = true;
            }
            else if (Paranoia > ALARM_LEVEL)
            {
                if (!pause)
                {
                    if (!alarmSound.isPlaying)
                    {
                        alarmSound.Play();
                    }
                }
                else
                {
                    alarmSound.Stop();
                }
            }
            else
            {
                alarmSound.Stop();
            }

            if (bgTweenAlpha.GetComponent<tk2dSprite>().color.a == 0)
            {
                bgTweenAlpha.Fade(0, 1, .012f);
            }
        }
        else if (gameOver && !done)
        {
            levelStats.Money = money;

            alarmSound.Stop();
            backgroundMusic.Stop();

            gameOverController.GameOver();

            doExistSound.Play();
            gameOverSound.PlayDelayed(3f);

            done = true;
        }
        else if (win && !done)
        {
            levelStats.Money = money;

            alarmSound.Stop();
            backgroundMusic.Stop();

            gameOverController.Win();

            PlayWinSound();
            winSound.PlayDelayed(3f);

            done = true;
        }
        else
        {
            alarmSound.Stop();
        }
	}

    public Elevator GetElevator(int key)
    {
        Elevator elevator;
        elevators.TryGetValue(key, out elevator);
        return elevator;
    }

    public Floor GetFloor(int key)
    {
        Floor floor;
        floors.TryGetValue(key, out floor);
        return floor;
    }

    public void AddPoints(PerformanceText.PerformanceEnum perf)
    {
        switch (perf)
        {
            case PerformanceText.PerformanceEnum.NICENICE:
                money += NICE_NICE_REWARD;
                break;
            case PerformanceText.PerformanceEnum.FANTASTIC:
                money += FANTASTIC_REWARD;
                break;
            case PerformanceText.PerformanceEnum.ACCEPTABLE:
                money += ACCEPTABLE_REWARD;
                break;
            case PerformanceText.PerformanceEnum.SLOPPY:
                money += SLOPPY_REWARD;
                break;
        }
    }

    public void AddParanoia(float addParanoia)
    {
        paranoia += addParanoia;
    }

    public void RemoveParanoia(float removeParanoia)
    {
        paranoia -= removeParanoia;
    }

    protected void IncrementTime()
    {
        dt += Time.deltaTime;

        if (dt > 1)
        {
            time += INCREMENT_TIME;
            tens += INCREMENT_TIME;
            if (tens == 60)
            {
                time -= tens;
                time += 100;
                tens = 0;
            }
            dt = 0;
        }
    }

    protected void SetCurrentRate()
    {
        if ((time < SIX && time >= FIVE))
        {
            patronSendDelay = BASE_RATE;
            spiderSendDelay = DEAD_RATE * 5;
        }
        else if ((time < SEVEN && time >= SIX) ||
            (time < ELEVEN && time >= TEN) ||
            (time < FIFTEEN && time >= FOURTEEN))
        {
            patronSendDelay = NORMAL_RATE;
            spiderSendDelay = BASE_RATE;
        }
        else if ((time < EIGHT && time >= SEVEN) ||
            (time < TEN && time >= NINE) ||
            (time < TWELVE && time >= ELEVEN) ||
            (time < FOURTEEN && time >= THIRTEEN) ||
            (time < SEVENTEEN && time >= SIXTEEN) ||
            (time < NINETEEN && time >= EIGHTEEN))
        {
            patronSendDelay = BUSY_RATE;
            spiderSendDelay = DEAD_RATE * 2;
        }
        else if ((time < NINE && time >= EIGHT) ||
                (time < THIRTEEN && time >= TWELVE) ||
                (time < EIGHTEEN && time >= SEVENTEEN))
        {
            patronSendDelay = OVERWHELM_RATE;
            spiderSendDelay = DEAD_RATE;
        }
        else if (time < SIXTEEN && time >= FIFTEEN)
        {
            patronSendDelay = DEAD_RATE;
            spiderSendDelay = OVERWHELM_RATE;
        }
        else
        {
            win = true;
        }

    }

    protected void PlayWinSound()
    {
        if (levelStats.NumSloppy > 10)
        {
            sloppySound.Play();
        }
        else if (levelStats.NumAcceptable > 10)
        {
            acceptableSound.Play();
        }
        else if (levelStats.NumFantastic > 10 && levelStats.NumFantastic > levelStats.NumNiceNice)
        {
            fantasticSound.Play();
        }
        else if (levelStats.NumNiceNice > 10)
        {
            niceNiceSound.Play();
        }
        else
        {
            acceptableSound.Play();
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpeechBubbleController : MonoBehaviour {

    protected const float LENGTH_VISIBLE = 1.5f;

    protected tk2dTextMesh text;
    protected tk2dSprite sprite;
    protected tk2dSpriteAnimator anim;
    protected TweenAlpha alphaTween;

    protected float dt;
    protected bool visible;

	// Use this for initialization
	void Start () 
    {
        alphaTween = GetComponent<TweenAlpha>();
        text = this.transform.FindChild("SpeechBubbleText").GetComponent<tk2dTextMesh>();
        sprite = this.transform.FindChild("SpeechBubbleSprite").GetComponent<tk2dSprite>();
        anim = this.transform.FindChild("SpeechBubbleSprite").GetComponent<tk2dSpriteAnimator>();
        visible = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (visible)
        {
            dt += Time.deltaTime;
            if (dt > LENGTH_VISIBLE)
            {
                dt = 0;
                alphaTween.FadeOut();
                visible = false;
            }
        }
	}

    public void Appear(string newText, int type, MonoBehaviour attach)
    {
        anim.Play("Large" + type);
        //sprite.scale = new Vector3(2f, 2f, 1);
        alphaTween.FadeIn();
        this.transform.position = new Vector3(attach.transform.position.x, attach.transform.position.y + 32, transform.parent.position.z - .3f);
        this.transform.parent = attach.transform;
        text.text = newText;
        text.Commit();
        visible = true;
    }

    public void Appear(int newInt, int type, MonoBehaviour attach)
    {
        anim.Play("Small" + type);
        alphaTween.FadeIn();
        this.transform.position = new Vector3(attach.transform.position.x, attach.transform.position.y + 32, transform.parent.position.z - .3f);
        this.transform.parent = attach.transform;
        text.text = newInt.ToString();
        text.Commit();
        visible = true;
    }
}

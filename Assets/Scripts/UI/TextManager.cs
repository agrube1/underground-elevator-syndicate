﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextManager : MonoBehaviour {

    protected const int MAX_NODE = 10;

    public Transform performance;
    protected Transform guiLayer;

    protected PerformanceText text;
    protected List<PerformanceText> pool;
    protected int currentNode;

	// Use this for initialization
	void Start () 
    {
        guiLayer = this.transform.Find("GuiFarLayer");
        text = performance.GetComponent<PerformanceText>();

        pool = new List<PerformanceText>();
        for (int i = 0; i < MAX_NODE; i++)
        {
            GameObject perfText = (GameObject)Instantiate(text.gameObject);
            perfText.transform.parent = guiLayer;
            perfText.name = "Performance Text " + i;
            pool.Add(perfText.GetComponent<PerformanceText>());
        }
	}

    // Update is called once per frame
    void Update() 
    {
	
	}

    public void DisplayText(PerformanceText.PerformanceEnum perf)
    {
        pool[currentNode].Display(perf);
        currentNode++;
        if (currentNode >= MAX_NODE)
        {
            currentNode = 0;
        }
    }
}

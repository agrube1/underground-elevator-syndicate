﻿using UnityEngine;
using System.Collections;

public class ExitButtonController : TouchObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override bool OnTap(GestureObject g)
    {
        Application.Quit();
        return true;
    }

    public override bool OnPull(GestureObject g)
    {
        Application.Quit();
        return base.OnPull(g);
    }
}

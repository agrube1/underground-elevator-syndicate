﻿using UnityEngine;
using System.Collections;

public class RetryButtonController : TouchObject {

    protected LevelProperties levelProperties;

    // Use this for initialization
    void Start()
    {
        levelProperties = transform.parent.parent.parent.GetComponent<LevelProperties>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override bool OnTap(GestureObject g)
    {
        if (levelProperties.GameOver || levelProperties.Win)
        {
            Application.LoadLevel("GameScene");
            return true;
        }
        return false;
    }

    public override bool OnPull(GestureObject g)
    {
        if (levelProperties.GameOver || levelProperties.Win)
        {
            Application.LoadLevel("GameScene");
            return true;
        }
        return false;
    }
}

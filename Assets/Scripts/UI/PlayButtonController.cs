﻿using UnityEngine;
using System.Collections;

public class PlayButtonController : TouchObject {

    protected TweenAlpha tweenAlpha;
    protected tk2dSpriteAnimator anim;
    protected tk2dSprite sprite;

	// Use this for initialization
	void Start () 
    {
        tweenAlpha = transform.parent.GetComponent<TweenAlpha>();
        anim = GetComponent<tk2dSpriteAnimator>();
        sprite = GetComponent<tk2dSprite>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (sprite.color.a == 0)
        {
            Application.LoadLevel("GameScene");
        }
	}

    public override bool OnTap(GestureObject g)
    {
        anim.Play("Up");
        tweenAlpha.FadeOut();
        return true;
    }

    public override bool OnPull(GestureObject g)
    {
        anim.Play("Up");
        tweenAlpha.FadeOut();
        return true;
    }
}

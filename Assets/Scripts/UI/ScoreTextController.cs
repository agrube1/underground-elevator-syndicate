﻿using UnityEngine;
using System.Collections;

public class ScoreTextController : MonoBehaviour {

    protected tk2dTextMesh score;
    protected LevelProperties levelProperties;

    protected float lastScore;

	// Use this for initialization
	void Start () 
    {
        levelProperties = this.transform.parent.parent.GetComponent<LevelProperties>();
        score = this.transform.FindChild("ScoreText").GetComponent<tk2dTextMesh>();
        lastScore = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (lastScore != levelProperties.Money)
            {
                score.text = levelProperties.Money.ToString("c2");
                score.Commit();
            }
        }
	}
}

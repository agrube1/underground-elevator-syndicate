﻿using UnityEngine;
using System.Collections;

public class PerformanceText : TouchObject
{

    #region Color Constants
    protected Color NN_RGB;
    protected Color NN_RGB2;

    protected Color F_RGB;
    protected Color F_RGB2;

    protected Color A_RGB;
    protected Color A_RGB2;

    protected Color S_RGB;
    protected Color S_RGB2;

    #endregion

    protected const float FADE_IN_RATE = 3;
    protected const float FADE_OUT_RATE = 100;

    public static Vector3 HIDE_POSITION = new Vector3(450, 520, -9);
    public static Vector3 PLACEMENT_POSITION = new Vector3(450, 350, -9);

    public enum PerformanceEnum
    {
        NICENICE,
        FANTASTIC,
        ACCEPTABLE,
        SLOPPY
    }

    protected bool activated;


    protected tk2dTextMesh text;
    protected TweenAlpha alphaTween;
    protected TweenMovement movementTween;
    protected float time;

	// Use this for initialization
	void Start () 
    {
        HIDE_POSITION.z = transform.parent.position.z;
        PLACEMENT_POSITION.z = transform.parent.position.z;
        activated = false;
        text = ((Transform)transform.FindChild("PerformanceText")).GetComponent<tk2dTextMesh>();
        transform.position = PLACEMENT_POSITION;

        alphaTween = transform.GetComponent<TweenAlpha>();
        movementTween = transform.GetComponent<TweenMovement>();

        time = 0;

        NN_RGB = new Color(0, 1, 0);
        NN_RGB2 = new Color(0, .37f, 0);

        F_RGB = new Color(1, 1, .18f);
        F_RGB2 = new Color(.01f, .435f, .031f);

        A_RGB = new Color(1, .878f, 0);
        A_RGB2 = new Color(1, .075f, .424f);

        S_RGB = new Color(1, 0, 0);
        S_RGB2 = new Color(.451f, 0, 0);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (activated)
        {
            if (alphaTween.alpha == 1)
            {
                movementTween.StartMove(HIDE_POSITION, .01f, 1);
            }

            if (transform.position.Equals(HIDE_POSITION))
            {
                activated = false;
                alphaTween.rate = 100;
                alphaTween.FadeOut();
            }
        }
        
	}

    public void Display(PerformanceEnum perf)
    {
        if (perf == PerformanceEnum.NICENICE)
        {
            text.text = "NICENICE";
            text.color = NN_RGB;
            text.color2 = NN_RGB2;
        }
        else if (perf == PerformanceEnum.FANTASTIC)
        {
            text.text = "FANTASTIC";
            text.color = F_RGB;
            text.color2 = F_RGB2;
        }
        else if (perf == PerformanceEnum.ACCEPTABLE)
        {
            text.text = "ACCEPTABLE";
            text.color = A_RGB;
            text.color2 = A_RGB2;
        }
        else
        {
            text.text = "SLOPPY";
            text.color = S_RGB;
            text.color2 = S_RGB2;
        }
        text.Commit();
        
        transform.position = PLACEMENT_POSITION;
        activated = true;

        alphaTween.rate = 3;
        alphaTween.FadeIn();
    }
}

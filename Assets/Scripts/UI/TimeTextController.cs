﻿using UnityEngine;
using System.Collections;

public class TimeTextController : MonoBehaviour {

    protected tk2dTextMesh time;
    protected LevelProperties levelProperties;

    protected int lastTime;

    // Use this for initialization
    void Start()
    {
        levelProperties = this.transform.parent.parent.GetComponent<LevelProperties>();
        time = this.transform.FindChild("TimeText").GetComponent<tk2dTextMesh>();
        lastTime = levelProperties.GameTime;
        time.text = "Time: " + levelProperties.GameTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (lastTime != levelProperties.GameTime)
            {
                time.text = "Time: " + levelProperties.GameTime;
                time.Commit();
            }
        }
    }
}

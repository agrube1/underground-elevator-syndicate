﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpeechBubbleManager : MonoBehaviour {

    protected const int MAX_BUBBLES = 10;

    public Transform speechBubble;
    protected Transform guiLayer;

    protected SpeechBubbleController bubble;
    protected List<SpeechBubbleController> pool;
    protected int currentNode;

    protected Dictionary<int, SpeechBubble> activeSpeechBubbles;

    // Use this for initialization
    void Start()
    {
        guiLayer = guiLayer = this.transform.Find("GuiFarLayer");

        bubble = speechBubble.GetComponent<SpeechBubbleController>();

        pool = new List<SpeechBubbleController>();
        for (int i = 0; i < MAX_BUBBLES; i++)
        {
            GameObject newSpeechBubble = (GameObject)Instantiate(bubble.gameObject);
            newSpeechBubble.transform.parent = guiLayer;
            newSpeechBubble.name = "Speech Bubble " + i;
            pool.Add(newSpeechBubble.GetComponent<SpeechBubbleController>());
        }

        activeSpeechBubbles = new Dictionary<int, SpeechBubble>();
        for (int i = 0; i < 10; i++)
        {
            activeSpeechBubbles.Add(i, new SpeechBubble());
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void DisplayBubble(string newText, int type, MonoBehaviour attach)
    {
        pool[currentNode].Appear(newText, type, attach);
        currentNode++;
        if (currentNode >= MAX_BUBBLES)
        {
            currentNode = 0;
        }
    }

    public bool DisplayBubble(string newText, int type, int elevatorNum, PatronController attach)
    {
        if (!activeSpeechBubbles[elevatorNum].isActive)
        {
            pool[currentNode].Appear(newText, type, attach);
            activeSpeechBubbles[elevatorNum].isActive = true;
            activeSpeechBubbles[elevatorNum].controller = pool[currentNode];
            currentNode++;
            if (currentNode >= MAX_BUBBLES)
            {
                currentNode = 0;
            }
            return true;
        }
        else
        {
            // wait until the last speech bubble was completed
            TweenAlpha tweenAlpha = activeSpeechBubbles[elevatorNum].controller.GetComponent<TweenAlpha>();
            if (tweenAlpha.alpha <= 0
                && tweenAlpha.To == tweenAlpha.alpha)
            {
                activeSpeechBubbles[elevatorNum].isActive = false;
            }
            return false;
        }
    }

    public void DisplayBubble(int newInt, int type, MonoBehaviour attach)
    {
        pool[currentNode].Appear(newInt, type, attach);
        currentNode++;
        if (currentNode >= MAX_BUBBLES)
        {
            currentNode = 0;
        }
    }

    public bool DisplayBubble(int newInt, int type, int elevatorNum, PatronController attach)
    {
        if (!activeSpeechBubbles[elevatorNum].isActive)
        {
            pool[currentNode].Appear(newInt, type, attach);
            activeSpeechBubbles[elevatorNum].isActive = true;
            activeSpeechBubbles[elevatorNum].controller = pool[currentNode];
            currentNode++;
            if (currentNode >= MAX_BUBBLES)
            {
                currentNode = 0;
            }
            return true;
        }
        else
        {
            // wait until the last speech bubble was completed
            TweenAlpha tweenAlpha = activeSpeechBubbles[elevatorNum].controller.GetComponent<TweenAlpha>();
            if (tweenAlpha.alpha <= 0 
                && tweenAlpha.To == tweenAlpha.alpha)
            {
                activeSpeechBubbles[elevatorNum].isActive = false;
            }
            return false;
        }
    }
}

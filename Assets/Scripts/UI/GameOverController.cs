﻿using UnityEngine;
using System.Collections;

public class GameOverController : MonoBehaviour {

    protected LevelStats levelStats;

    protected TweenAlpha tweenAlpha;
    protected tk2dTextMesh scoreText;
    protected tk2dTextMesh niceNiceText;
    protected tk2dTextMesh fantasticText;
    protected tk2dTextMesh acceptableText;
    protected tk2dTextMesh sloppyText;
    protected tk2dTextMesh spiderText;
    protected tk2dTextMesh moleText;
    protected tk2dSpriteAnimator anim;

	// Use this for initialization
	void Start () {
        levelStats = transform.parent.parent.GetComponent<LevelStats>();
        tweenAlpha = GetComponent<TweenAlpha>();

        scoreText = this.transform.Find("ScoreText").GetComponent<tk2dTextMesh>();
        niceNiceText = this.transform.Find("NiceNiceText").GetComponent<tk2dTextMesh>();
        fantasticText = this.transform.Find("FantasticText").GetComponent<tk2dTextMesh>();
        acceptableText = this.transform.Find("AcceptableText").GetComponent<tk2dTextMesh>();
        sloppyText = this.transform.Find("SloppyText").GetComponent<tk2dTextMesh>();
        spiderText = this.transform.Find("SpidersText").GetComponent<tk2dTextMesh>();
        moleText = this.transform.Find("MoleMenText").GetComponent<tk2dTextMesh>();

        anim = this.transform.Find("GameOverSprite").GetComponent<tk2dSpriteAnimator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void GameOver()
    {
        UpdateStats();
        
        anim.Play("Lose");
        tweenAlpha.FadeIn();
    }

    public void Win()
    {
        UpdateStats();

        anim.Play("Win");
        tweenAlpha.FadeIn();
    }

    protected void UpdateStats()
    {
        scoreText.text = "Earnings: " + levelStats.Money.ToString("c2");
        scoreText.Commit();
        niceNiceText.text = levelStats.NumNiceNice.ToString();
        niceNiceText.Commit();
        fantasticText.text = levelStats.NumFantastic.ToString();
        fantasticText.Commit();
        acceptableText.text = levelStats.NumAcceptable.ToString();
        acceptableText.Commit();
        sloppyText.text = levelStats.NumSloppy.ToString();
        sloppyText.Commit();
        spiderText.text = levelStats.NumSpiders.ToString();
        spiderText.Commit();
        moleText.text = levelStats.NumMoleMen.ToString();
        moleText.Commit();
    }
}

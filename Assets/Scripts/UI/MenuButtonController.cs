﻿using UnityEngine;
using System.Collections;

public class MenuButtonController : TouchObject 
{

    protected LevelProperties levelProperties;

    // Use this for initialization
    void Start()
    {
        levelProperties = transform.parent.parent.parent.GetComponent<LevelProperties>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override bool OnTap(GestureObject g)
    {
        if (levelProperties.GameOver || levelProperties.Win)
        {
            Application.LoadLevel("MenuScene");
            return true;
        }
        return false;
    }

    public override bool OnPull(GestureObject g)
    {
        if (levelProperties.GameOver || levelProperties.Win)
        {
            Application.LoadLevel("MenuScene");
            return true;
        }
        return false;
    }
}

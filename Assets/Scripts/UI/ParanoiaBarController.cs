﻿using UnityEngine;
using System.Collections;

public class ParanoiaBarController : MonoBehaviour 
{
    protected Color RED;
    protected Color YELLOW;
    protected Color GREEN;

    protected tk2dSprite barSprite;
    protected tk2dSprite orbSprite;
    protected LevelProperties levelProperties;

    protected Vector3 scale;

	// Use this for initialization
	void Start () 
    {
        RED = new Color(.8f, .1f, .1f);
        YELLOW = new Color(.8f, .8f, .1f);
        GREEN = new Color(.1f, .8f, .1f);
        levelProperties = transform.parent.parent.GetComponent<LevelProperties>();
        barSprite = this.transform.Find("ParanoiaBarSprite").Find("BarSprite").GetComponent<tk2dSprite>();
        orbSprite = this.transform.Find("ParanoiaBarSprite").Find("OrbSprite").GetComponent<tk2dSprite>();
        scale = new Vector3(barSprite.scale.x, barSprite.scale.y, barSprite.scale.z);
	}
	
	// Update is called once per frame
	void Update () 
    {
        scale.x = Mathf.Min(1, levelProperties.Paranoia / LevelProperties.MAX_PARANOIA);
        barSprite.scale = scale;

        if (scale.x > LevelProperties.MEDIUM_MAX)
        {
            barSprite.color = RED;
            orbSprite.color = RED;
        }
        else if (scale.x > LevelProperties.SAFE_MAX)
        {
            barSprite.color = YELLOW;
            orbSprite.color = YELLOW;
        }
        else
        {
            barSprite.color = GREEN;
            orbSprite.color = GREEN;
        }
	}
}

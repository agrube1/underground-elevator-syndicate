﻿using UnityEngine;
using System.Collections;

public class LizardController : MonoBehaviour {

    protected enum LizardState
    {
        REPAIR,
        MOVING,
        IDLE
    }

    protected LizardState state;
    protected LizardState nextState;

    public int elevatorNum;

    protected LevelProperties levelProperties;
    protected Elevator elevator;
    protected TweenMovement movement;
    protected tk2dSpriteAnimator anim;
    protected AudioSource repairSound;
    protected Vector3 initialPosition;

	// Use this for initialization
	void Start () 
    {
        levelProperties = transform.parent.parent.GetComponent<LevelProperties>();
        elevator = levelProperties.GetElevator(elevatorNum);
        movement = transform.GetComponent<TweenMovement>();
        anim = this.transform.FindChild("LizardSprite").GetComponent<tk2dSpriteAnimator>();
        repairSound = this.transform.FindChild("RepairSound").GetComponent<AudioSource>();
        initialPosition = new Vector3(0, transform.position.y, transform.position.z);
        state = LizardState.IDLE;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            movement.enabled = true;
            switch (state)
            {
                case LizardState.IDLE:
                    if (elevator.IsAt(0) && elevator.Health < Elevator.MAX_HEALTH)
                    {
                        ToRepair();
                    }
                    else
                    {
                        Idle();
                    }
                    break;
                case LizardState.MOVING:
                    if (!movement.IsMoving)
                    {
                        ToNext();
                    }
                    break;
                case LizardState.REPAIR:
                    if (!elevator.IsAt(0) || elevator.Health >= Elevator.MAX_HEALTH)
                    {
                        ToIdle();
                    }
                    else
                    {
                        Repair();
                    }
                    break;
            }
        }
        else
        {
            movement.enabled = false;
        }
	}

    protected void ToRepair()
    {
        float x = 0;
        if (initialPosition.x - elevator.nodes[0].NodePosition.x < 0)
        {
            x = elevator.nodes[0].NodePosition.x - 64;
        }
        else
        {
            x = elevator.nodes[0].NodePosition.x + 64;
        }
        movement.StartConstantMove(new Vector3(x, this.transform.position.y, transform.position.z), new Vector3(100, 0, 0));
        state = LizardState.MOVING;
        nextState = LizardState.REPAIR;
        anim.Play("Walk");
    }

    protected void Repair()
    {
        anim.Play("Repair");
        if (!repairSound.isPlaying)
        {
            repairSound.Play();
        }
        elevator.AddHealth(Elevator.HEALTH_RATE * Time.deltaTime);
    }

    protected void ToIdle()
    {
        repairSound.Stop();
        initialPosition.x = elevator.nodes[0].NodePosition.x / 5;
        movement.StartConstantMove(initialPosition, new Vector3(100, 0, 0));
        state = LizardState.MOVING;
        nextState = LizardState.IDLE;
        anim.Play("Walk");
    }

    protected void Idle()
    {
        anim.Play("Idle");
    }

    protected void ToNext()
    {
        state = nextState;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatronPool : MonoBehaviour {

    protected const int MAX_PATRONS = 30;
    protected const float SEND_DELAY = 2f;

    public Transform patron;
    protected Transform patronLayer;

    protected LevelProperties levelProperties;

    protected List<PatronController> pool;
    protected int currentNode;

    protected float dt;

	// Use this for initialization
	void Start () 
    {
        patronLayer = this.transform.Find("PatronLayer");

        levelProperties = GetComponent<LevelProperties>();

        pool = new List<PatronController>();
        for (int i = 0; i < MAX_PATRONS; i++)
        {
            Transform patronObj = (Transform)Instantiate(patron);
            patronObj.transform.parent = patronLayer;
            patronObj.name = "Patron " + i;
            pool.Add(patronObj.GetComponent<PatronController>());
        }
        dt = 0;
        currentNode = 0;
	}

    // Update is called once per frame
    void Update() 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            dt += Time.deltaTime;
            if (dt > levelProperties.PatronRate)
            {
                SendEnemy();
            }
        }
	}

    protected void SendEnemy()
    {
        if (pool[currentNode].State == PatronController.PatronState.INACTIVE)
        {
            pool[currentNode].Send();
            dt = 0;
        }
        currentNode++;
        if (currentNode >= MAX_PATRONS)
        {
            currentNode = 0;
        }
    }
}

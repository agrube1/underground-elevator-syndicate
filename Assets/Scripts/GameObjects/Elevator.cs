﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Elevator : TouchObject
{

    public enum ElevatorState
    {
        AT_FLOOR,
        MOVING,
        MOVE_TO_FLOOR,
        FALLING,
        BROKEN,
        ATTACKING,
        INACTIVE
    }

    public enum ElevatorDirection
    {
        HORIZONTAL,
        VERTICAL
    }

    #region Constants
    protected const float EXTRA_SPACE = 64;

    protected const float NICENICE = 5;
    protected const float FANTASTIC = 8;
    protected const float ACCEPTABLE = 16;
    protected const float SLOPPY = 20;


    public const float MAX_HEALTH = 10;
    public const float HEALTH_RATE = 5;
    #endregion

    protected float health;
    protected float dt;

    protected ElevatorDirection direction;
    public List<Node> nodes;
    protected Dictionary<int, int> numOccupantMap;

    // used to stored what index in the array corresponds to what floor
    public Dictionary<int, int> nodeFloorMap;

    protected PerformanceText.PerformanceEnum lastPerformance;

    protected ElevatorState state;

    #region Properties
    protected int elevatorNumber;
    public int ElevatorNumber
    {
        get { return elevatorNumber; }
    }

    public float X
    {
        get { return nodes[0].NodePosition.x; }
    }

    protected int numOccupants;
    public int NumOccupants
    {
        set { numOccupants = value; }
    }

    protected bool IsOccupied
    {
        get { return numOccupants > 0; }
    }

    public bool IsMoving
    {
        get { return movement.IsMoving; }
    }

    public float Health
    {
        get { return health; }
    }

    public PerformanceText.PerformanceEnum LastPerformance
    {
        get { return lastPerformance; }
    }

    public TweenMovement.Direction YDirection
    {
        get { return movement.YDirection; }
    }

    public ElevatorState State
    {
        get { return state; }
    }

    #endregion

    #region Components
    protected TweenMovement movement;
    protected Transform cachedTransform;
    protected LevelProperties levelProperties;
    protected LevelStats levelStats;
    protected AudioSource elevatorDing;
    protected AudioSource elevatorFall;
    protected AudioSource elevatorCrash;
    protected AudioSource elevatorHit;
    protected tk2dSpriteAnimator sparksAnim;
    protected tk2dSprite sparksSprite;
    #endregion

	// Use this for initialization
	void Start () 
    {
        direction = ElevatorDirection.VERTICAL;
        movement = GetComponent<TweenMovement>();
        cachedTransform = GetComponent<Transform>();
        numOccupantMap = new Dictionary<int, int>();
        levelProperties = this.transform.parent.parent.GetComponent<LevelProperties>();
        levelStats = this.transform.parent.parent.GetComponent<LevelStats>();
        elevatorDing = transform.Find("ElevatorBell").GetComponent<AudioSource>();
        elevatorCrash = transform.Find("ElevatorCrash").GetComponent<AudioSource>();
        elevatorFall = transform.Find("ElevatorFall").GetComponent<AudioSource>();
        elevatorHit = transform.Find("ElevatorHit").GetComponent<AudioSource>();
        sparksAnim = transform.Find("SparkSprite").GetComponent<tk2dSpriteAnimator>();
        sparksSprite = transform.Find("SparkSprite").GetComponent<tk2dSprite>();

        state = ElevatorState.AT_FLOOR;
        numOccupants = 0;
        health = MAX_HEALTH;
        dt = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            switch (state)
            {
                case ElevatorState.FALLING:
                    if (this.transform.position.Equals2D(nodes[0].NodePosition))
                    {
                        sparksSprite.color = new Color(sparksSprite.color.r, sparksSprite.color.g,
                                                       sparksSprite.color.b, 0);
                        sparksAnim.Stop();
                        elevatorFall.Stop();
                        elevatorCrash.Play();
                        state = ElevatorState.BROKEN;
                    }
                    break;
                case ElevatorState.ATTACKING:
                    dt += Time.deltaTime;
                    if (dt > .1f)
                    {
                        state = ElevatorState.AT_FLOOR;
                        movement.enabled = true;
                        dt = 0;
                    }
                    break;
                case ElevatorState.MOVE_TO_FLOOR:
                    // cannot drag it again until it has reached a destination                    
                    if (!IsMoving && health > 0)
                    {
                        //play elevator ding
                        elevatorDing.Play();
                        state = ElevatorState.AT_FLOOR;
                    }
                    break;
            }
        }
	}

    public void Board(Vector3 position)
    {
        Node closest = FindNode(position);
        if (!numOccupantMap.ContainsKey(closest.FloorNumber))
        {
            numOccupantMap.Add(closest.FloorNumber, 0);
        }
        numOccupantMap[closest.FloorNumber]++;
        closest.ElevatorLight.Activate();
        numOccupants++;
    }

    public override bool OnDrag(GestureObject g)
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            // only be able to move it if it's not moving to a station
            if (state == ElevatorState.MOVING || state == ElevatorState.AT_FLOOR)
            {
                Vector3 moveTo;
                if (direction == ElevatorDirection.VERTICAL)
                {
                    float y = g.EndPosition.y;
                    if (g.EndPosition.y > nodes[nodes.Count - 1].NodePosition.y + EXTRA_SPACE)
                    {
                        y = nodes[nodes.Count - 1].NodePosition.y + EXTRA_SPACE;
                    }
                    else if (g.EndPosition.y < nodes[0].NodePosition.y - EXTRA_SPACE)
                    {
                        y = nodes[0].NodePosition.y - EXTRA_SPACE;
                    }
                    moveTo = new Vector3(cachedTransform.position.x, y, cachedTransform.position.z);
                }
                else
                {
                    float x = g.EndPosition.x;
                    if (g.EndPosition.x > nodes[nodes.Count - 1].NodePosition.x)
                    {
                        x = nodes[nodes.Count - 1].NodePosition.x;
                    }
                    else if (g.EndPosition.x < nodes[0].NodePosition.x)
                    {
                        x = nodes[0].NodePosition.x;
                    }
                    moveTo = new Vector3(x, cachedTransform.position.y, cachedTransform.position.z);
                }
                // tween to the position that the drag is
                movement.StartMove(moveTo, TweenMovement.MovementType.SMOOTH, 1.0f, 0.0f);

                state = ElevatorState.MOVING;
                return true;
            }
            return false;
        }
        return false;
    }

    public override bool OnPull(GestureObject g)
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (state == ElevatorState.MOVING)
            {
                KeyValuePair<float, Node> closestNode = FindClosestNode();

                // create the entry in the dictionary if it's not there before attempting to access it.
                if (!numOccupantMap.ContainsKey(closestNode.Value.FloorNumber))
                {
                    numOccupantMap.Add(closestNode.Value.FloorNumber, 0);
                }

                if (IsOccupied && numOccupantMap[closestNode.Value.FloorNumber] > 0)
                {
                    PerformanceText.PerformanceEnum perf = DisplayPerformanceText(closestNode.Key);
                    for (int i = 0; i < numOccupantMap[closestNode.Value.FloorNumber]; i++)
                    {
                        levelProperties.AddPoints(perf);
                        numOccupants--;
                    }
                    closestNode.Value.ElevatorLight.Deactivate();
                    numOccupantMap[closestNode.Value.FloorNumber] = 0;
                }

                movement.StartMove(closestNode.Value.NodePosition, .1f, 1f);

                state = ElevatorState.MOVE_TO_FLOOR;

                return true;

            }
            return false;
        }
        return false;
    }

    protected KeyValuePair<float, Node> FindClosestNode()
    {
        float smallestDistance = float.MaxValue;
        Node closest = null;

        foreach (Node n in nodes)
        {
            float distance = Mathf.Abs(Vector3.Distance(n.NodePosition, cachedTransform.position));
            if (distance < smallestDistance)
            {
                closest = n;
                smallestDistance = distance;
            }
        }

        return new KeyValuePair<float, Node>(smallestDistance, closest);

    }

    protected Node FindNode(Vector3 position)
    {
        float smallestDistance = float.MaxValue;
        Node closest = null;

        foreach (Node n in nodes)
        {
            float distance = Mathf.Abs(Vector3.Distance(n.NodePosition, position));
            if (distance < smallestDistance)
            {
                closest = n;
                smallestDistance = distance;
            }
        }

        return closest;

    }

    protected PerformanceText.PerformanceEnum DisplayPerformanceText(float distance)
    {
        PerformanceText.PerformanceEnum perf = PerformanceText.PerformanceEnum.SLOPPY;
        if (distance <= NICENICE)
        {
            perf = PerformanceText.PerformanceEnum.NICENICE;
            levelStats.NumNiceNice++;
        }
        else if (distance <= FANTASTIC)
        {
            perf = PerformanceText.PerformanceEnum.FANTASTIC;
            levelStats.NumFantastic++;
        }
        else if (distance <= ACCEPTABLE)
        {
            perf = PerformanceText.PerformanceEnum.ACCEPTABLE;
            levelStats.NumAcceptable++;
        }
        else
        {
            perf = PerformanceText.PerformanceEnum.SLOPPY;
            levelStats.NumSloppy++;
        }
        
        transform.parent.parent.GetComponent<TextManager>().DisplayText(perf);
        lastPerformance = perf;
        return perf;
    }

    protected void GameOver()
    {
        state = ElevatorState.INACTIVE;
        movement.Cancel();
    }

    public void RemoveHealth(float subtract)
    {
        if (state != ElevatorState.ATTACKING)
        {
            health -= subtract;
            if (health <= 0)
            {
                health = 0;
                state = ElevatorState.FALLING;

                if (transform.position.y != nodes[0].NodePosition.y)
                {
                    sparksSprite.color = new Color(sparksSprite.color.r, sparksSprite.color.g,
                                                   sparksSprite.color.b, 1);
                    sparksAnim.Play("Spark");
                    elevatorFall.Play();
                    movement.StartConstantMove(nodes[0].NodePosition, new Vector3(0, 400, 0));
                }
            }
        }
    }

    public void AddHealth(float add)
    {
        state = ElevatorState.AT_FLOOR;
        health += add;
        if (health >= MAX_HEALTH)
        {
            health = MAX_HEALTH;
        }
    }

    public bool IsAt(int num)
    {
        return this.transform.position.Equals2D(nodes[num].NodePosition);
    }

    void OnTriggerStay(Collider collider)
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (collider.CompareTag("Enemy") && movement.YDirection == TweenMovement.Direction.DOWN 
                && state == ElevatorState.MOVING)
            {
                state = ElevatorState.ATTACKING;
                elevatorHit.Play();
            }
        }
    }

}

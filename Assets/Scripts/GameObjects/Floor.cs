﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Floor {

    public Node rightNode;
    public Node leftNode;
    public int floorNumber;

    public float Y
    {
        get 
        {
            if (rightNode != null) return rightNode.NodePosition.y;
            else return 0;
        }
    }

    public Floor()
    {
        
    }

    public Vector3 GetClosestEndPosition(Vector3 position)
    {
        return (Vector3.Distance(position, rightNode.NodePosition) <= Vector3.Distance(position, leftNode.NodePosition) 
                ? rightNode.NodePosition : leftNode.NodePosition);
    }
}

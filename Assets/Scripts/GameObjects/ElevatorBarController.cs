﻿using UnityEngine;
using System.Collections;

public class ElevatorBarController : MonoBehaviour {

    protected const float GREEN_HEALTH = .6f;
    protected const float YELLOW_HEALTH = .3f;

    protected Color RED;
    protected Color YELLOW;
    protected Color GREEN;

    protected LevelProperties levelProperties;
    protected Elevator elevator;
    protected tk2dSprite sprite;

    protected Vector3 scale;

	// Use this for initialization
	void Start () 
    {
        RED = new Color(.8f, .1f, .1f);
        YELLOW = new Color(.8f, .8f, .1f);
        GREEN = new Color(.1f, .8f, .1f);
        levelProperties = transform.parent.GetComponent<LevelProperties>();
        elevator = this.transform.parent.GetComponent<Elevator>();
        sprite = this.transform.Find("ElevatorHealthBarSprite").GetComponent<tk2dSprite>();
        scale = new Vector3(sprite.scale.x, sprite.scale.y, sprite.scale.z);
	}
	
	// Update is called once per frame
	void Update () 
    {
        scale.x = Mathf.Min(1, elevator.Health / Elevator.MAX_HEALTH );
        sprite.scale = scale;

        if (scale.x < YELLOW_HEALTH)
        {
            sprite.color = RED;
        }
        else if (scale.x < GREEN_HEALTH)
        {
            sprite.color = YELLOW;
        }
        else
        {
            sprite.color = GREEN;
        }	
	}
}

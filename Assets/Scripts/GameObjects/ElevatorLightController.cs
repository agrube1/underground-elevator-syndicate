﻿using UnityEngine;
using System.Collections;

public class ElevatorLightController : MonoBehaviour {

    protected tk2dSprite sprite;
    protected tk2dSpriteAnimator anim;

	// Use this for initialization
	void Start () 
    {
        anim = transform.Find("ElevatorLightSprite").GetComponent<tk2dSpriteAnimator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void Activate()
    {
        anim.Play("On");
    }

    public void Deactivate()
    {
        anim.Play("Off");
    }
}

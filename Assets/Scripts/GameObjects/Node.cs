﻿using UnityEngine;
using System.Collections;

public class Node
{
    protected int floorNumber;
    public int FloorNumber
    {
        get { return floorNumber; }
        set { floorNumber = value; }
    }

    protected int elevatorNumber;
    public int ElevatorNumber
    {
        get { return elevatorNumber; }
        set { elevatorNumber = value; }
    }

    protected Vector3 nodePosition;
    public Vector3 NodePosition
    {
        get { return nodePosition; }
        set { nodePosition = value; }
    }

    protected ElevatorLightController light;
    public ElevatorLightController ElevatorLight
    {
        get { return light; }
        set { light = value; }
    }

    public Node(Vector3 nodePosition, int elevatorNumber, int floorNumber)
    {
        this.nodePosition = nodePosition;
        this.elevatorNumber = elevatorNumber;
        this.floorNumber = floorNumber;
    }

}

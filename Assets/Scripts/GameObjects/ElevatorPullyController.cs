﻿using UnityEngine;
using System.Collections;

public class ElevatorPullyController : MonoBehaviour {

    public Elevator elevator;
    protected tk2dSpriteAnimator anim;

	// Use this for initialization
	void Start () 
    {
        anim = transform.FindChild("ElevatorPullySprite").GetComponent<tk2dSpriteAnimator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (elevator.IsMoving)
        {
            anim.Play("Pully");
        }
        else
        {
            anim.Stop();
        }
	}
}

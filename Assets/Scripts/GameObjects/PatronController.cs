﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PatronController : MonoBehaviour 
{
    protected const float ADD_PARANOIA = 2;
    protected const float WAIT_OFFSET = 60;

    public enum PatronState
    {
        INACTIVE,
        MOVE_TO_ELEVATOR,
        WAITING,
        BOARDED,
        MOVE_TO_EXIT,
        EXIT
    }

    protected PatronState state;

    public PatronState State
    {
        get { return state; }
    }

    protected int elevator;
    protected bool leftSide;

    protected float paranoia;

    protected LevelProperties levelProperties;
    protected SpeechBubbleManager speechBubbles;
    protected Elevator targetElevator;
    protected TweenMovement movement;
    protected tk2dSprite sprite;
    protected tk2dSpriteAnimator anim;

    protected Transform patronLayer;

    protected Queue<int> floors;
    protected Queue<Vector3> path;
    protected Vector3 nextMove;
    protected Vector3 nextWait;
    protected Vector3 movementVelocity;

    protected bool saidFloor;

    protected int patronNum;

	// Use this for initialization
	void Start () 
    {
        path = new Queue<Vector3>();
        floors = new Queue<int>();

        movement = GetComponent<TweenMovement>();
        levelProperties = this.transform.parent.parent.GetComponent<LevelProperties>();
        speechBubbles = this.transform.parent.parent.GetComponent<SpeechBubbleManager>();
        transform.position = new Vector3(1000, 0, transform.parent.position.z);
        sprite = this.transform.FindChild("PatronSprite").GetComponent<tk2dSprite>();
        anim = this.transform.FindChild("PatronSprite").GetComponent<tk2dSpriteAnimator>();

        movementVelocity = new Vector3(100, 0, 0);
        state = PatronState.INACTIVE;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            movement.enabled = true;
            switch (state)
            {
                case PatronState.EXIT:
                    Exit();
                    break;
                case PatronState.MOVE_TO_EXIT:
                    if (transform.position.Equals2D(nextMove))
                    {
                        state = PatronState.EXIT;
                    }
                    break;
                case PatronState.BOARDED:
                    if (!transform.parent.position.Equals2D(nextMove))
                    {
                        if (!saidFloor)
                        {
                            SayFloor();
                        }
                        AddParanoia();
                    }
                    else
                    {
                        Serve();
                    }
                    break;
                case PatronState.WAITING:
                    if (targetElevator.transform.position.Equals2D(nextMove))
                    {
                        state = PatronState.MOVE_TO_ELEVATOR;
                        anim.Play("Walk" + patronNum);
                    }
                    else
                    {
                        AddParanoia();
                    }
                    break;
                case PatronState.MOVE_TO_ELEVATOR:
                    if (targetElevator.transform.position.Equals2D(nextMove))
                    {
                        movement.StartConstantMove(nextMove, movementVelocity);
                        if (transform.position.Equals2D(nextMove))
                        {

                            Board();
                            anim.Play("Wait" + patronNum);
                        }
                    }
                    else
                    {
                        movement.StartConstantMove(nextWait, movementVelocity);
                        if (transform.position.Equals2D(nextWait))
                        {
                            Wait();
                            anim.Play("Wait" + patronNum);
                        }
                    }

                    break;
            }
        }
        else
        {
            movement.enabled = false;
        }
	}

    public void Send()
    {
        int enterSide = Random.Range(-1, 1);
        int rand = Random.Range(0, 65);
        if (rand < 20)
        {
            patronNum = 1;
        }
        else if (rand < 40)
        {
            patronNum = 2;
        }
        else if (rand < 60)
        {
            patronNum = 3;
        }
        else
        {
            patronNum = 4;
        }
        leftSide = enterSide == -1;

        int elevatorNumber = Random.Range(0, levelProperties.NumElevators);
        targetElevator = levelProperties.GetElevator(elevatorNumber);

        // the start floor generator
        // randomly generate a floor that exists in the elevators path. 
        // COULD BE IMPROVED BY WRITING RANDOM FUNCTION THAT PICKS FROM nodeFloorMap
        int startFloorNumber = Random.Range(1, levelProperties.NumFloors);
        while (!targetElevator.nodeFloorMap.ContainsValue(startFloorNumber))
            startFloorNumber = Random.Range(1, levelProperties.NumFloors);
        // get the floor and position the patron
        Floor startFloor = levelProperties.GetFloor(startFloorNumber);
        Vector3 startPosition = new Vector3();
        startPosition.x = leftSide ? startFloor.leftNode.NodePosition.x : startFloor.rightNode.NodePosition.x;
        startPosition.y = leftSide ? startFloor.leftNode.NodePosition.y : startFloor.rightNode.NodePosition.y;
        startPosition.z = transform.parent.position.z;
        transform.position = startPosition;

        patronLayer = transform.parent;

        // the end floor generator
        // randomly generate a floor that exists in the elevators path and is not the start floor 
        // COULD BE IMPROVED BY WRITING RANDOM FUNCTION THAT PICKS FROM nodeFloorMap
        int endFloorNumber = startFloorNumber;
        while (endFloorNumber == startFloorNumber || !targetElevator.nodeFloorMap.ContainsValue(endFloorNumber))
            endFloorNumber = Random.Range(1, levelProperties.NumFloors);
        Floor endFloor = levelProperties.GetFloor(endFloorNumber);
        Vector3 targetFloorPosition = new Vector3(targetElevator.X, endFloor.Y, 0);
        Vector3 endPosition = endFloor.GetClosestEndPosition(targetFloorPosition);
        floors.Enqueue(endFloorNumber);

        // enqueue all the locations that this patron will move
        path.Enqueue(new Vector3(targetElevator.X, startFloor.Y, transform.parent.position.z));
        path.Enqueue(targetFloorPosition);
        path.Enqueue(endPosition);

        GetNextMove();

        state = PatronState.MOVE_TO_ELEVATOR;
        saidFloor = false;

        movement.StartConstantMove(nextMove, movementVelocity);
        anim.Play("Walk" + patronNum);
    }

    protected void SayFloor()
    {
        int floorNumber = floors.Peek();
        bool wait = speechBubbles.DisplayBubble(floorNumber, patronNum, targetElevator.ElevatorNumber, this);
        if (wait)
        {
            saidFloor = true;
            floorNumber = floors.Dequeue();
        }
    }

    protected void SayRemark(string remark)
    {
        speechBubbles.DisplayBubble(remark, patronNum, this);
    }

    protected void Wait()
    {
        state = PatronState.WAITING;
        // play waiting animation
    }

    protected void Board()
    {
        nextMove = path.Dequeue();
        targetElevator.Board(nextMove);
        transform.parent = targetElevator.transform;
        state = PatronState.BOARDED;
    }

    protected void Serve()
    {
        transform.parent = patronLayer;

        if (path.Count > 1)
        {
            GetNextMove();

            state = PatronState.MOVE_TO_ELEVATOR;
            movement.StartConstantMove(nextMove, new Vector3(100, 0, 0));
        }
        else if (path.Count == 1)
        {
            nextMove = path.Dequeue();
            state = PatronState.MOVE_TO_EXIT;
            movement.StartConstantMove(nextMove, new Vector3(100, 0, 0));
        }
        float removeParanoia;
        switch (targetElevator.LastPerformance)
        {
            case PerformanceText.PerformanceEnum.NICENICE:
                removeParanoia = paranoia;
                //SayRemark("NICENICE!");
                break;
            case PerformanceText.PerformanceEnum.FANTASTIC:
                removeParanoia = paranoia * .9f;
                break;
            case PerformanceText.PerformanceEnum.ACCEPTABLE:
                removeParanoia = paranoia * .6f;
                //SayRemark("That was a bumpy ride!");
                break;
            case PerformanceText.PerformanceEnum.SLOPPY:
                removeParanoia = paranoia * .2f;
                break;
            default:
                removeParanoia = 0;
                break;
        }
        levelProperties.RemoveParanoia(removeParanoia);
        paranoia -= removeParanoia;
        anim.Play("Walk" + patronNum);
    }

    protected void Exit()
    {
        state = PatronState.INACTIVE;
        transform.position = new Vector3(700, transform.position.y, transform.parent.position.z);
    }

    protected void AddParanoia()
    {
        float addParanoia = 0;
        
        if (this.transform.position.Equals2D(targetElevator.nodes[0].NodePosition))
        {
            addParanoia = ADD_PARANOIA * 2 * Time.deltaTime;
        }
        else
        {
            addParanoia = ADD_PARANOIA * Time.deltaTime;
        }
        paranoia += addParanoia;
        levelProperties.AddParanoia(addParanoia);
    }

    protected void RemoveParanoia()
    {
        levelProperties.RemoveParanoia(paranoia);
    }

    protected void GetNextMove()
    {
        nextMove = path.Dequeue();
        if (transform.position.x > nextMove.x)
        {
            nextWait = new Vector3(nextMove.x + WAIT_OFFSET, nextMove.y, nextMove.z);
        }
        else
        {
            nextWait = new Vector3(nextMove.x - WAIT_OFFSET, nextMove.y, nextMove.z);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpiderController : MonoBehaviour {

    public enum SpiderState
    {
        MOVING,
        ATTACKING,
        STUNNED,
        DEAD
    }

	public Vector3 startPosition;
	protected LevelProperties levelProperties;
    protected LevelStats levelStats;
    protected TweenMovement movement;
    protected AudioSource biteSound;

    protected Vector3 stunImpulse;
    protected Vector3 stunTarget;
    protected Vector3 deathPosition;
    protected Vector3 moveTo;
    protected Vector3 velocity;

	protected Elevator myElevator;
	protected float timeSince;

    public SpiderState state;
	
	// Use this for initialization
	void Start () {
        state = SpiderState.DEAD;
		
		levelProperties = this.transform.parent.parent.GetComponent<LevelProperties>();
        levelStats = this.transform.parent.parent.GetComponent<LevelStats>();
        biteSound = transform.FindChild("BiteSound").GetComponent<AudioSource>();
        movement = GetComponent<TweenMovement>();

        stunImpulse = new Vector3();
        stunTarget = new Vector3();
        deathPosition = new Vector3();
        moveTo = new Vector3();
        velocity = new Vector3(0, 150, 0);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (state != SpiderState.DEAD)
            {
                if (state == SpiderState.STUNNED)
                {
                    stunTarget.x = this.transform.position.x;
                    stunTarget.y = stunImpulse.y - 150;
                    stunTarget.z = this.transform.position.z;

                    movement.StartMove(stunTarget, TweenMovement.MovementType.SMOOTH, .1f, 5f);

                    if (timeSince > 0.5f)
                        state = SpiderState.MOVING;

                    timeSince += Time.deltaTime;
                }
                else if (state == SpiderState.MOVING)
                {
                    moveTo.x = myElevator.transform.position.x;
                    moveTo.y = myElevator.transform.position.y - 64;
                    moveTo.z = myElevator.transform.position.z;

                    movement.StartConstantMove(myElevator.transform.position, velocity);
                }
                else if (state == SpiderState.ATTACKING)
                {
                    myElevator.RemoveHealth(Elevator.HEALTH_RATE * 0.9f * Time.deltaTime);
                    if (!biteSound.isPlaying)
                    {
                        biteSound.Play();
                    }
                    if (Mathf.Abs(myElevator.transform.position.y - this.transform.position.y) >
                        myElevator.gameObject.collider.bounds.extents.y * 2)
                    {
                        state = SpiderState.MOVING;
                    }
                }
                else
                {
                    state = SpiderState.MOVING;
                }

                if (this.transform.position.y < -500 && state != SpiderState.DEAD)
                {
                    ToDead();
                }
            }
        }
	}
	
	void OnTriggerStay(Collider collider) 
    {
        if (!levelProperties.Done && !levelProperties.Pause)
        {
            if (myElevator != null)
            {
                if (myElevator.State == Elevator.ElevatorState.FALLING)
                {
                    state = SpiderState.DEAD;
                    deathPosition.x = this.transform.position.x;
                    deathPosition.y = -600;
                    deathPosition.z = this.transform.position.z;
                    movement.StartConstantMove(deathPosition, 5 * velocity);
                }
                else if (myElevator.name.Equals(collider.transform.name) 
                    && !myElevator.IsMoving && state != SpiderState.STUNNED)
                {
                    state = SpiderState.ATTACKING;
                }
                else if (myElevator.name.Equals(collider.transform.name) 
                    && myElevator.IsMoving && myElevator.YDirection == TweenMovement.Direction.DOWN
                    && myElevator.State == Elevator.ElevatorState.ATTACKING)
                {
                    stunImpulse.x = this.transform.position.x;
                    stunImpulse.y = this.transform.position.y - 200;
                    stunImpulse.z = this.transform.position.z;

                    movement.StartMove(stunImpulse, TweenMovement.MovementType.SMOOTH, .3f, 5f);

                    state = SpiderState.STUNNED;
                    timeSince = 0.0f;
                }
            }
        }
	}
	
	public void Send()
	{
		myElevator = levelProperties.GetElevator(Random.Range(0, levelProperties.NumElevators));
		this.transform.position = myElevator.transform.position;

        state = SpiderState.MOVING;
		this.transform.position = new Vector3(myElevator.transform.position.x, -450, transform.parent.transform.position.z);
	}

    protected void ToDead()
    {
        state = SpiderState.DEAD;

        deathPosition.x = this.transform.position.x;
        deathPosition.y = -600;
        deathPosition.z = this.transform.position.z;

        movement.StartMove(deathPosition, TweenMovement.MovementType.SMOOTH, .3f, 5f);
        levelStats.NumSpiders++;
    }
}

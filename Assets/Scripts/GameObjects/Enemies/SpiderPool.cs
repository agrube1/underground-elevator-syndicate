﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpiderPool : MonoBehaviour {

    protected int MaxSpiders = 5;
    protected const float MIN_DELAY = 13f;
    protected const float MAX_DELAY = 15f;

    protected float nextDelay;

    public Transform spider;
    protected Transform enemyLayer;
    protected LevelProperties levelProperties;
    
    protected List<SpiderController> pool;
    protected int currentNode;

    protected float dt;

	// Use this for initialization
	void Start () 
    {
        levelProperties = GetComponent<LevelProperties>();
        enemyLayer = this.transform.Find("EnemyLayer");

        pool = new List<SpiderController>();
        for (int i = 0; i < MaxSpiders; i++)
        {
            Transform spiderObj = (Transform)Instantiate(spider);
            spiderObj.transform.parent = enemyLayer;
            spiderObj.transform.position = new Vector3(0, 1000, transform.parent.position.z);
            spiderObj.name = "Spider " + i;
            pool.Add(spiderObj.GetComponent<SpiderController>());
        }
        dt = 0;
        currentNode = 0;
        GenerateNextDelay();
	}

    // Update is called once per frame
    void Update() 
    {
        dt += Time.deltaTime;
        if (dt > levelProperties.SpiderRate)
        {
            SendEnemy();
        }
	}

    protected void SendEnemy()
    {
        if (pool[currentNode].state == SpiderController.SpiderState.DEAD)
        {
            pool[currentNode].Send();
            dt = 0;
        }
        currentNode++;
        if (currentNode >= MaxSpiders)
        {
            currentNode = 0;
        }

        GenerateNextDelay();
    }

    protected void GenerateNextDelay()
    {
        nextDelay = Random.Range(MIN_DELAY, MAX_DELAY);
    }
}


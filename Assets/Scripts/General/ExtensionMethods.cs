﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods 
{
    public static bool Equals2D(this Vector3 me, Vector3 other)
    {
        return me.x == other.x && me.y == other.y;
    }
}

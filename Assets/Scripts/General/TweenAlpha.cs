﻿using UnityEngine;
using System.Collections;

public class TweenAlpha : MonoBehaviour {

    public float rate = 5.0f;
    protected float t;
    protected Color color;

    protected float to;
    public float To
    {
        get { return to; }
    }

    protected float from;

    protected bool fading;

    protected tk2dBaseSprite sprite;

    public float alpha
    {
        get { return color.a; }
        set 
        {
            color.a = value;
            if (sprite != null)
            {
                sprite.color = color;
            }
            foreach (Transform child in transform)
            {
                tk2dTextMesh childText = child.GetComponent<tk2dTextMesh>();
                if (childText != null)
                {
                    if (childText.useGradient)
                    {
                        childText.color2 = new Color(childText.color2.r, childText.color2.g, childText.color2.b, alpha);
                        childText.Commit();
                    }
                    childText.color = new Color(childText.color.r, childText.color.g, childText.color.b, alpha);
                    childText.Commit();

                }

                tk2dBaseSprite childSprite = child.GetComponent<tk2dBaseSprite>();
                if (childSprite != null)
                {
                    childSprite.color = color;
                }
            }
        }
    }

	// Use this for initialization
	void Start () 
    {
        fading = false;
        sprite = GetComponent<tk2dBaseSprite>();
        color = new Color(1, 1, 1, 0);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (fading)
        {
            t += Time.deltaTime * rate;
            alpha = Mathf.Lerp(from, to, t);
            if (alpha == to)
            {
                fading = false;
                t = 0;
            }
        }
	}

    public void Fade(float from, float to, float rate)
    {
        this.from = from;
        this.to = to;
        this.rate = rate;
        fading = true;
    }

    public void FadeOut()
    {
        from = 1;
        to = 0;
        fading = true;
    }

    public void FadeIn()
    {
        from = 0;
        to = 1;
        fading = true;
    }

    public void PartialFadeOut()
    {
        from = 1;
        to = .15f;
        fading = true;
    }
}

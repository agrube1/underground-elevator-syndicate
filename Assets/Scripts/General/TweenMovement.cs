﻿using UnityEngine;
using System.Collections;

public class TweenMovement : MonoBehaviour {

    public enum MovementType
    {
        LERP,
        SMOOTH,
        LINEAR,
        CONSTANT_VELOCITY,
        PARABOLIC
    }

    public enum Direction
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    protected Vector3 moveTo;
    public MovementType movementType;
    protected float rate;
    protected float finalDelta;
    
    protected Vector3 delta;
    protected Vector3 startPosition;
    protected Vector3 velocity;
    
    protected bool isMoving;
    protected bool isPaused;
    
    protected float t;

    #region Components
    
    protected Transform cachedTransform;
    protected Vector3 position;
    #endregion

    public bool IsMoving
    {
        get { return isMoving; }
    }

    public Direction XDirection
    {
        get { return (startPosition.x < moveTo.x ? Direction.RIGHT : Direction.LEFT); }
    }

    public Direction YDirection
    {
        get { return (startPosition.y < moveTo.y ? Direction.UP : Direction.DOWN); }
    }

    public Vector3 Velocity
    {
        get { return velocity; }
    }

    // Use this for initialization
	void Start () 
    {
        cachedTransform = transform;
        position = cachedTransform.position;
        delta = new Vector3();
        startPosition = new Vector3();
        t = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        if (isMoving && !isPaused)
        {
            if (movementType == MovementType.SMOOTH)
            {
                if (CheckFinalThreshold())
                    return;

                delta.x = cachedTransform.position.x + (moveTo.x - cachedTransform.position.x) * rate;
                delta.y = cachedTransform.position.y + (moveTo.y - cachedTransform.position.y) * rate;
                delta.z = cachedTransform.position.z;
                cachedTransform.position = delta;
            }
            else if (movementType == MovementType.LERP)
            {
                if (CheckFinalThreshold())
                    return;

                t += Time.deltaTime * rate;
                delta.x = Mathf.Lerp(cachedTransform.position.x, moveTo.x, t);
                delta.y = Mathf.Lerp(cachedTransform.position.y, moveTo.y, t);
                delta.z = cachedTransform.position.z;
                cachedTransform.position = delta;
            }
            else if (movementType == MovementType.LINEAR)
            {
                if (CheckFinalThreshold())
                    return;

                t += Time.deltaTime * rate;
                delta.x = Mathf.Lerp(startPosition.x, moveTo.x, t);
                delta.y = Mathf.Lerp(startPosition.y, moveTo.y, t);
                delta.z = cachedTransform.position.z;
                cachedTransform.position = delta;
            }
            else if (movementType == MovementType.CONSTANT_VELOCITY)
            {
                if (CheckFinalConstantThreshold())
                    return;

                t += Time.deltaTime * rate;
                delta.x = cachedTransform.position.x + velocity.x * Time.deltaTime;
                delta.y = cachedTransform.position.y + velocity.y * Time.deltaTime;
                delta.z = cachedTransform.position.z;
                cachedTransform.position = delta;
            }
            //else if (movementType == MovementType.PARABOLIC)
            //{
            //    if (Mathf.Abs(cachedTransform.position.x - moveTo.x) < finalDelta
            //     && Mathf.Abs(cachedTransform.position.y - moveTo.y) < finalDelta)
            //    {
            //        cachedTransform.position = new Vector3(moveTo.x, moveTo.y, cachedTransform.position.z);
            //        isMoving = false;
            //        t = 0;
            //        return;
            //    }

            //    t += Time.deltaTime * rate;
            //    delta.x = Mathf.Lerp(cachedTransform.position.x, moveTo.x, t);
            //    delta.y = Mathf.Lerp(cachedTransform.position.y, moveTo.y, t);
            //    delta.z = cachedTransform.position.z;
            //    cachedTransform.position = delta;
            //}
        }
	}

    public void StartMove(Vector3 moveTo, float rate, float finalDelta)
    {
        this.moveTo = moveTo;
        this.rate = rate;
        this.finalDelta = finalDelta;
        this.isMoving = true;
        this.startPosition = cachedTransform.position;
    }

    public void StartMove(Vector3 moveTo, MovementType type, float rate, float finalDelta)
    {
        this.moveTo = moveTo;
        this.rate = rate;
        this.finalDelta = finalDelta;
        this.isMoving = true;
        this.movementType = type;
        this.startPosition = cachedTransform.position;
    }

    public void StartConstantMove(Vector3 moveTo, Vector3 velocity)
    {
        this.moveTo = moveTo;
        // make the velocity go in the direction of the moveTo.
        this.velocity = AddDirection(moveTo, velocity);
        this.finalDelta = 5f;
        this.isMoving = true;
        this.movementType = MovementType.CONSTANT_VELOCITY;
        this.startPosition = cachedTransform.position;
    }

    void OnPauseEnter()
    {
        isPaused = true;
    }

    void OnPauseExit()
    {
        isPaused = false;
    }

    protected bool CheckFinalThreshold()
    {
        if (Mathf.Abs(cachedTransform.position.x - moveTo.x) <= finalDelta
         && Mathf.Abs(cachedTransform.position.y - moveTo.y) <= finalDelta)
        {
            position.x = moveTo.x;
            position.y = moveTo.y;
            position.z = cachedTransform.position.z;
            cachedTransform.position = position;
            isMoving = false;
            t = 0;
            return true;
        }

        return false;
    }

    protected bool CheckFinalConstantThreshold()
    {
        if ((velocity.x != 0 ? Mathf.Abs(cachedTransform.position.x - moveTo.x) <= finalDelta : true)
         && (velocity.y != 0 ? Mathf.Abs(cachedTransform.position.y - moveTo.y) <= finalDelta : true))
        {
            position.x = moveTo.x;
            position.y = moveTo.y;
            position.z = cachedTransform.position.z;
            cachedTransform.position = position;
            isMoving = false;
            t = 0;
            return true;
        }

        return false;
    }

    protected Vector3 AddDirection(Vector3 moveTo, Vector3 velocity)
    {
        Vector3 position = transform.position;
        if (position.x > moveTo.x)
        {
            velocity.x = -velocity.x;
        }

        if (position.y > moveTo.y)
        {
            velocity.y = -velocity.y;
        }

        return velocity;

    }

    public void Cancel()
    {
        moveTo = cachedTransform.position;
        isMoving = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class FlipManager : MonoBehaviour {

    protected TweenMovement movement;
    protected tk2dSprite sprite;

    protected Vector3 scale;

	// Use this for initialization
	void Start () 
    {
        movement = GetComponent<TweenMovement>();
        string spriteName = this.name.Split(' ')[0];
        sprite = this.transform.FindChild(spriteName + "Sprite").GetComponent<tk2dSprite>();
        scale = new Vector3();
	}
	
	// Update is called once per frame
	void Update () 
    {
        CheckHorizontalFlip();
	}

    protected void CheckHorizontalFlip()
    {
        if (movement.XDirection == TweenMovement.Direction.LEFT)
        {
            if (sprite.scale.x > 0)
            {
                scale.x = -sprite.scale.x;
                scale.y = sprite.scale.y;
                scale.z = sprite.scale.z;

                sprite.scale = scale;
            }
        }
        else if (movement.XDirection == TweenMovement.Direction.RIGHT)
        {
            if (sprite.scale.x < 0)
            {
                scale.x = -sprite.scale.x;
                scale.y = sprite.scale.y;
                scale.z = sprite.scale.z;

                sprite.scale = scale;
            }
        }
    }
}

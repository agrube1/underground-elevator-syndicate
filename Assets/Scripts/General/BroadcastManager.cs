﻿using UnityEngine;
using System.Collections;

public class BroadcastManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void BroadcastToAll(string message, object value)
    {
        GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in gos)
        {
            if (go)
            {
                go.SendMessage(message, value, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
